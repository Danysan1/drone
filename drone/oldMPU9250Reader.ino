#include "oldMPU9250Reader.h"
#include "config.h"

#define MPU 0x68

float oldMPU9250Reader::FunctionsPitchRoll(double A, double B, double C) {
  float DatoA, DatoB, Value;
  DatoA = A;
  DatoB = (B * B) + (C * C);
  DatoB = sqrt(DatoB);

  Value = atan2(DatoA, DatoB);
  Value = Value * 180 / M_PI;

  return Value;
}

oldMPU9250Reader::oldMPU9250Reader () {
  Wire.begin(); // join i2c bus (address optional for master)

  Wire.beginTransmission(MPU);
  Wire.write(0x6B);  // PWR_MGMT_1 register
  Wire.write(0);     // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);
}

struct angoli oldMPU9250Reader::getAngoli() {
  struct angoli a;
  int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ;

  Wire.beginTransmission(MPU);
  Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU, 14, true); // request a total of 14 registers

  AcX = Wire.read() << 8 | Wire.read(); // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
  AcY = Wire.read() << 8 | Wire.read(); // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ = Wire.read() << 8 | Wire.read(); // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  Tmp = Wire.read() << 8 | Wire.read(); // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
  GyX = Wire.read() << 8 | Wire.read(); // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
  GyY = Wire.read() << 8 | Wire.read(); // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
  GyZ = Wire.read() << 8 | Wire.read(); // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)

  a.pitch  = FunctionsPitchRoll(AcY, AcX, AcZ);
  a.roll = FunctionsPitchRoll(AcX, AcY, AcZ);
  a.yaw = 0; // TODO

#ifdef DEBUG_GYRO
  Serial.print("GYRO Pitch: ");
  Serial.print(a.pitch);
  Serial.print("\t Roll: ");
  Serial.print(a.roll);
  Serial.print("\t Yaw: ");
  Serial.println(a.yaw);
#endif

  return a;

}
