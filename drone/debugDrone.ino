#include "debugDrone.h"
#include "config.h"

void setupDebug(){
  Serial.begin(9600);
  Serial.println("\nSeriale pronta per debug");
}

void stampaJoystick(struct joystick j){
	#ifdef DEBUG_RICEZIONE
    Serial.print("JOYSTICK Throttle: ");
    Serial.print(j.throttle);
    Serial.print("\t Pitch: ");
    Serial.print(j.pitch);
    Serial.print("\t Roll: ");
    Serial.print(j.roll);
    Serial.print("\t Yaw: ");
    Serial.print(j.yaw);
    Serial.print("\t Calibrazione: ");
    Serial.println(j.calibrazione);
#endif
}

void stampaThrottle(struct throttle th){
	#ifdef DEBUG_MOTORI
  Serial.print("THROTTLE Up: ");
  Serial.print(th.up);
  Serial.print("\t Front: ");
  Serial.print(th.front);
  Serial.print("\t Right: ");
  Serial.print(th.right);
  Serial.print("\t Back: ");
  Serial.print(th.back);
  Serial.print("\t Left: ");
  Serial.println(th.left);
#endif
}