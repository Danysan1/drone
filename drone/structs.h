#ifndef STRUCTS_H
#define STRUCTS_H

struct joystick {
  short throttle, // Throttle generale
        pitch, roll, yaw; // Posizione obbiettivo
  bool calibrazione, // Fare calibrazione ESC
       control; // Abilitare il controllo PID
};
const struct joystick defaultJoystick = {0, 0, 0, 0, false, true};

struct angoli {
  float pitch, roll, yaw;
};

struct pid {
  int pitch, roll, yaw;
};
const struct pid defaultPID = {0, 0, 0};

struct throttle {
  int up, front, right, back, left;
};

struct telemetria {
  short pitch, roll, front, right, back, left;
};
struct telemetria getTelemetria(struct angoli a, struct throttle t) {
  return {(short)a.pitch, (short)a.roll, (short)t.front, (short)t.right, (short)t.back, (short)t.left};
}

#endif
