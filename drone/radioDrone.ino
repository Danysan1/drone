#include "radioDrone.h"
#include "structs.h"
#include "config.h"
#include "debugDrone.h"

#include <RF24.h>
#include <RF24_config.h>

RF24 radio(7, 8); // CE, CSN
unsigned long lastReceivedTime = 0, lastSentTime = 0;

void setupRadio() {
  Serial.println("Setup radio...");
  radio.begin();
  radio.openWritingPipe(0xF0F0F0F0A2);
  radio.openReadingPipe(1, 0xF0F0F0F0A1);
  radio.setPALevel(RF24_PA_MIN);
  radio.startListening();
  Serial.println(radio.isChipConnected() ? "Radio connesso" : "Radio non connesso");
}

struct joystick leggiRadio(struct joystick j) {
  if (radio.available()) {
    radio.read(&j, sizeof(j));

		stampaJoystick(j);

    lastReceivedTime = millis();
  } else if (millis() - lastReceivedTime > RADIO_TIMEOUT) // Timeout radio
    j = defaultJoystick;

  return j;
}

void svuotaRadio(){
  struct joystick j;
  
  while (radio.available()) {
    radio.read(&j, sizeof(j));
  }
}

void inviaTelemetria(struct telemetria tel) {
#ifdef INVIO_TELEMETRIA
  if (millis() - lastSentTime > INTERVALLO_TELEMETRIA) {
    radio.stopListening();
    radio.write(&tel, sizeof(tel));
    radio.startListening();
    lastSentTime = millis();
  }
#endif
}
