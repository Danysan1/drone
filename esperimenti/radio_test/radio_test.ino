#include <nRF24L01.h>
#include <printf.h>
#include <RF24.h>
#include <RF24_config.h>

#define DEBUG

struct joystick {
  int throttle, pitch, roll, yaw;
};

struct angoli {
  double pitch, roll, yaw;
};

struct slaveData {
  joystick j;
  angoli a;
};

RF24 radio(7, 8); // CE, CSN
const byte address[6] = "00001";
slaveData s = {{0, 0, 0, 0}, {0, 0, 0}};

void setup() {
#ifdef DEBUG
  Serial.begin(9600);
  Serial.println("\n###############     Caricamento in corso...     ###############");
#endif

  radio.begin();
  radio.openReadingPipe(0, address);
  radio.setPALevel(RF24_PA_MIN);
  radio.startListening();

#ifdef DEBUG
  Serial.println(radio.isChipConnected() ? "Radio connesso" : "Radio non connesso");
  //radio.printDetails();
  Serial.println("###############     Caricamento completato     ###############");
#endif
}

void loop() {
  if (radio.available()) {
    radio.read(&s.j, sizeof(s.j));

    Serial.print("Js Pitch: ");
    Serial.print(s.j.pitch);
    Serial.print("\t Js Roll: ");
    Serial.print(s.j.roll);
    Serial.print("\t Js Yaw: ");
    Serial.print(s.j.yaw);
    Serial.print("\t Js Throttle: ");
    Serial.println(s.j.throttle);
  }

  delay(10);
}
