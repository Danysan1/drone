/**
 * Usage, according to documentation(https://www.firediy.fr/files/drone/HW-01-V4.pdf) : 
 *     1. Plug your Arduino to your computer with USB cable, open terminal, then type 1 to send max throttle to every ESC to enter programming mode
 *     2. Power up your ESCs. You must hear "beep1 beep2 beep3" tones meaning the power supply is OK
 *     3. After 2sec, "beep beep" tone emits, meaning the throttle highest point has been correctly confirmed
 *     4. Type 0 to send min throttle
 *     5. Several "beep" tones emits, which means the quantity of the lithium battery cells (3 beeps for a 3 cells LiPo)
 *     6. A long beep tone emits meaning the throttle lowest point has been correctly confirmed
 *     7. Type 2 to launch test function. This will send min to max throttle to ESCs to test them
 *
 * @author lobodol <grobodol@gmail.com>
 */
// ---------------------------------------------------------------------------
#include <Servo.h>
// ---------------------------------------------------------------------------
// Customize here pulse lengths as needed
#define MIN_PULSE_LENGTH 1000 // Minimum pulse length in �s
#define MAX_PULSE_LENGTH 2000 // Maximum pulse length in �s
// ---------------------------------------------------------------------------
Servo motor,motor2,motor3,motor4;;
char data;
// ---------------------------------------------------------------------------

/**
 * Initialisation routine
 */
void setup() {
    Serial.begin(9600);
    
    motor.attach(9, MIN_PULSE_LENGTH, MAX_PULSE_LENGTH);
    motor2.attach(8, MIN_PULSE_LENGTH, MAX_PULSE_LENGTH);
    motor3.attach(10, MIN_PULSE_LENGTH, MAX_PULSE_LENGTH);
    motor4.attach(11, MIN_PULSE_LENGTH, MAX_PULSE_LENGTH);
    motor.writeMicroseconds(MIN_PULSE_LENGTH);
    motor2.writeMicroseconds(MIN_PULSE_LENGTH);
    motor3.writeMicroseconds(MIN_PULSE_LENGTH);
    motor4.writeMicroseconds(MIN_PULSE_LENGTH);
    displayInstructions();
}

/**
 * Main function
 */
void loop() {
    if (Serial.available()) {
        data = Serial.read();

        switch (data) {
            // 0
            case 48 : Serial.println("Sending minimum throttle");
                      motor.writeMicroseconds(MIN_PULSE_LENGTH);
    motor2.writeMicroseconds(MIN_PULSE_LENGTH);
    motor3.writeMicroseconds(MIN_PULSE_LENGTH);
    motor4.writeMicroseconds(MIN_PULSE_LENGTH);
            break;

            // 1
            case 49 : Serial.println("Sending maximum throttle");
                      motor.writeMicroseconds(MAX_PULSE_LENGTH);
    motor2.writeMicroseconds(MAX_PULSE_LENGTH);
    motor3.writeMicroseconds(MAX_PULSE_LENGTH);
    motor4.writeMicroseconds(MAX_PULSE_LENGTH);
            break;

            // 2
            case 50 : Serial.print("Running test in 3");
                      delay(1000);
                      Serial.print(" 2");
                      delay(1000);
                      Serial.println(" 1...");
                      delay(1000);
                      test();
            break;

            case 51 : Serial.println("Running test2");
                      test2();
            break;
        }
    }
    

}

/**
 * Test function: send min throttle to max throttle to each ESC.
 */
void test()
{
    for (int i = MIN_PULSE_LENGTH; i <= MAX_PULSE_LENGTH; i += 10) {
        Serial.print("Pulse length = ");
        Serial.println(i);
        motor.writeMicroseconds(i);
        motor2.writeMicroseconds(i);
        motor3.writeMicroseconds(i);
        motor4.writeMicroseconds(i);
        delay(200);
    }

    Serial.println("STOP");
    motor.writeMicroseconds(MIN_PULSE_LENGTH);
    motor2.writeMicroseconds(MIN_PULSE_LENGTH);
    motor3.writeMicroseconds(MIN_PULSE_LENGTH);
    motor4.writeMicroseconds(MIN_PULSE_LENGTH);
}

void test2()
{
        motor.writeMicroseconds(1200);
    motor2.writeMicroseconds(1200);
    motor3.writeMicroseconds(1200);
    motor4.writeMicroseconds(1200);
        delay(400);
        motor.writeMicroseconds(1600);
    motor2.writeMicroseconds(1600);
    motor3.writeMicroseconds(1600);
    motor4.writeMicroseconds(1600);
        delay(500);
        
        motor.writeMicroseconds(2000);
    motor2.writeMicroseconds(2000);
    motor3.writeMicroseconds(2000);
    motor4.writeMicroseconds(2000);
        delay(500);
        motor.writeMicroseconds(MIN_PULSE_LENGTH);
    motor2.writeMicroseconds(MIN_PULSE_LENGTH);
    motor3.writeMicroseconds(MIN_PULSE_LENGTH);
    motor4.writeMicroseconds(MIN_PULSE_LENGTH);
        

}

/**
 * Displays instructions to user
 */
void displayInstructions()
{  
    Serial.println("READY - PLEASE SEND INSTRUCTIONS AS FOLLOWING :");
    Serial.println("\t0 : Send min throttle");
    Serial.println("\t1 : Send max throttle");
    Serial.println("\t2 : Run test function");
    Serial.println("\t3 : Run test2 function\n");
}
