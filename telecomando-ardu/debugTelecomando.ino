#include "debugTelecomando.h"
#include "structs.h"
#include "config.h"

unsigned long lastTime;

void setupDebug() {
  Serial.begin(9600);

  //#ifdef DEBUG_SETUP
  Serial.println("Throttle,JS Pitch,JS Roll,JS Yaw,Pitch,Roll,Front,Right,Back,Left");
  //#endif
}

void stampaJoystick(struct joystick j) {
#ifdef DEBUG_JOYSTICK
  if (j.calibrazione) {
    Serial.print("##### CALIBRAZIONE #####");
    Serial.print(SEPARATORE);
    Serial.print(0);
    Serial.print(SEPARATORE);
    Serial.print(0);
    Serial.print(SEPARATORE);
    Serial.print(0);
  } else {
    Serial.print(map(j.throttle, THROTTLE_INVIO_MIN, THROTTLE_INVIO_MAX, 0, 100));
    Serial.print(SEPARATORE);

    if (!j.control) {
      Serial.print(0);
      Serial.print(SEPARATORE);
          
      Serial.print(0);
      Serial.print(SEPARATORE);
      
      Serial.print(0);
    } else {
      Serial.print(j.pitch);
      Serial.print(SEPARATORE);
      
      Serial.print(j.roll);
      Serial.print(SEPARATORE);
      
      Serial.print(j.yaw);
    }
  }
  Serial.print(SEPARATORE);
#endif
}

void stampaMap(short val, short minIn, short maxIn, short minOut, short maxOut){
  #ifdef TELEMETRIA_RAW
    Serial.print(val);
  #else
    Serial.print(map(val, minIn, maxIn, minOut, maxOut));
  #endif
}

void stampaTelemetria(struct telemetria t) {
#ifdef DEBUG_TELEMETRIA
  //Serial.print(t.roll);
  Serial.print(map(t.pitch, -45, 45, 100, 200));
  Serial.print(SEPARATORE);

  //Serial.print(t.roll);
  Serial.print(map(t.roll, -45, 45, 100, 200));
  Serial.print(SEPARATORE);

  //Serial.print(t.yaw);
  //Serial.print(map(t.yaw, -45, 45, 100, 200));
  //Serial.print(SEPARATORE);

  //Serial.print(t.front);
  Serial.print(map(t.front, MIN_THROTTLE, MAX_THROTTLE, 200, 300));
  Serial.print(SEPARATORE);

  //Serial.print(t.right);
  Serial.print(map(t.right, MIN_THROTTLE, MAX_THROTTLE, 200, 300));
  Serial.print(SEPARATORE);

  //Serial.print(t.back);
  Serial.print(map(t.back, MIN_THROTTLE, MAX_THROTTLE, 200, 300));
  Serial.print(SEPARATORE);

  //Serial.print(t.left);
  Serial.print(map(t.left, MIN_THROTTLE, MAX_THROTTLE, 200, 300));
  Serial.println();
#endif
}

void durata(const char* nome) {
#ifdef DEBUG_TEMPO
  unsigned long delta = millis() - lastTime;
  Serial.print('\t');
  Serial.print(delta);
  Serial.print(':');
  Serial.print(nome);
  Serial.print('\t');
  lastTime = millis();
#endif
}
